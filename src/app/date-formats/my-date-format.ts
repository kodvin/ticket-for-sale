export const MY_DATE_FORMATS = {
    parse: {
        // dateInput: {month: 'short', year: 'numeric', day: 'numeric'}
        dateInput: 'input'
    },
    display: {
        // dateInput: { month: 'numeric', year: 'numeric', day: 'numeric' },
        dateInput: 'input',
        monthYearLabel: {year: 'numeric', month: 'short'},
        dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
        monthYearA11yLabel: {year: 'numeric', month: 'long'},
    }
};