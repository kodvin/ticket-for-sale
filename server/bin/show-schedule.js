const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const path = require('path');
const moment = require('moment');
const constants = require('./constants');


function transformListToEventObjects(scheduleList) {
    scheduleList = scheduleList.map((show) => {
        let genreLowerCase = show[2].toLowerCase();
        return {
            'title': show[0],
            'startDateString': show[1],
            'genre': genreLowerCase
        }
    });

    return scheduleList;
}

function removeWhereStartDateIsAfterShowDate(scheduleListWithObjects, showDateString) {
    let listWithoutShowsInTheFuture = scheduleListWithObjects.reduce((accumulator, currentValue) => {
        let startDate = moment(currentValue.startDateString, constants.dateFormat, true);
        let showDate = moment(showDateString, constants.dateFormat, true);

        if (showDate.isAfter(startDate)) {
            accumulator.push(currentValue);
        }
        return accumulator;
    }, []);

    return listWithoutShowsInTheFuture;
}

module.exports = {
    getShowShedule(fileName, showDateString) {
        const filePath = path.join(__dirname, '../resources/' + fileName);
        const csvFileContent = fs.readFileSync(filePath, 'utf8');
        let scheduleListWithPropertyLists = parse(csvFileContent,
            {
                auto_parse: true, // Ensures that numeric values remain numeric
                delimiter: ',',
                quote: '"',
                relax: true,
                rowDelimiter: '\r\r\n', // file has extra \r in it
                skip_empty_lines: true
            }
        );
        let scheduleListWithObjects = transformListToEventObjects(scheduleListWithPropertyLists);
        scheduleListWithObjects = removeWhereStartDateIsAfterShowDate(scheduleListWithObjects, showDateString);

        return scheduleListWithObjects;
    }
};