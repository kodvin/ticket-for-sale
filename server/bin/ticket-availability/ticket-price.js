const constants = require('./../constants');

module.exports = class TicketPrice {
    constructor(genre) {
        this.genre = genre;
    }

    getPriceByGenre() {
        let pricesByGenre = constants.genrePricesDollars;

        for (let i = 0; i < pricesByGenre.length; i++) {
            if (pricesByGenre[i]['genre'] === this.genre.toLowerCase()) {
                return pricesByGenre[i]['priceInDollars'];
            }
        }
        //send an email? that there is a show with new genre
        return false;
    }

    getDiscountedPrice() {
        let priceInDollars = this.getPriceByGenre();
        let discount = priceInDollars * constants.discountInPercents / 100;
        let discountedPrice = priceInDollars - discount;
        let fixedDecimalDiscountedPrice = discountedPrice;

        return fixedDecimalDiscountedPrice;
    }

}