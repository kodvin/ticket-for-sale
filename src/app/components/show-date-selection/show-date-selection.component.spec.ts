import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDateSelectionComponent } from './show-date-selection.component';

describe('ShowDateSelectionComponent', () => {
  let component: ShowDateSelectionComponent;
  let fixture: ComponentFixture<ShowDateSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDateSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDateSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
