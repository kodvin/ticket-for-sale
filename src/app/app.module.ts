import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {TicketStateListComponent } from './components/ticket-state-list/ticket-state-list.component';
import {ApiService} from './services/api.service';
import {HttpClientModule} from '@angular/common/http';
//import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MY_DATE_FORMATS} from './date-formats/my-date-format';
import {MyDateAdapter} from './date-formats/my-date-adapter';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material';
import { ShowDateSelectionComponent } from './components/show-date-selection/show-date-selection.component';
import { TicketStateComponent } from './components/ticket-state/ticket-state.component';

@NgModule({
  declarations: [
    AppComponent,
    TicketStateListComponent,
    ShowDateSelectionComponent,
    TicketStateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
   // MatMomentDateModule,
  ],
  providers: [
    ApiService,
    {provide: DateAdapter, useClass: MyDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
