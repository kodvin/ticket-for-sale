const TicketState = require('../bin/ticket-availability/ticket-state');
const TicketDateState = require('../bin/ticket-availability/ticket-date-state');
const constants = require('../bin/constants');
const moment = require('moment');
const showsDb = require('../db/db');
const TicketAvailability = require('../bin/ticket-availability/ticket-availability');

exports.getTicketState = (req, res) => {
    let fileName = 'shows.csv';
    let queryDateString = moment().format(constants.dateFormat);
    let showDateString = req.params.showDate;
    if (moment(showDateString).isBefore(moment(), 'day')) {
        //show date cannot be in the past
        res.send([]);
        return;
    }
    let ticketStateGetter = new TicketState(fileName, queryDateString, showDateString);
    let stateWithPrices = ticketStateGetter.getStatesWithPrice();

    res.send(stateWithPrices);
};

exports.buyTickets = (req, res) => {
    let showDateString = req.body.showDateString;
    if (moment(showDateString).isBefore(moment(), 'day')) {
        res.send({status: false, message: "show date cannot be in the past"});
        return;
    }

    let title = req.body.title;
    let numberOfTickets = req.body.numberOfTickets;
    let startDateString = req.body.startDateString;
    let queryDateString = moment().format(constants.dateFormat);

    let ticketDateState = new TicketDateState(
        startDateString, queryDateString, showDateString
    );
    let ticketAvailability = new TicketAvailability(ticketDateState);
    let availableAmount = ticketAvailability.getAvailableTicketAmount();
    let alreadySoldTickets = showsDb.getSoldTickets(title, queryDateString, showDateString);
    let remainingTickets = availableAmount - alreadySoldTickets;
    let numberOfTicketsAfterPurchase = remainingTickets - numberOfTickets;
    let totalSold = alreadySoldTickets + numberOfTickets;

    if (numberOfTicketsAfterPurchase >= 0) {
        showsDb.saveSoldTickets(title, queryDateString, showDateString, totalSold);
        res.send({status: true, message: "Tickets bough successfully"});
    } else {
        res.send({status: false, message: "Cannot buy that amount of tickets"});

    }
};