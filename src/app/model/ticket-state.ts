export class TicketState {
    title: string;
    ticketsLeft: number;
    ticketsAvailable: number;
    genre: string;
    status: string;
} 