const express = require('express');
const router = express.Router();

const ticketController = require('../controllers/ticket-controller');

/* GET return ticket states */
router.get('/ticket/state/:showDate', ticketController.getTicketState);
/* POST attempt buying tickets */
router.post('/ticket/buy', ticketController.buyTickets);

module.exports = router;