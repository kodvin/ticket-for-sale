const schowScheduleGetter = require('./../show-schedule');
const TicketAvailability = require('./ticket-availability');
const TicketDateState = require('./ticket-date-state');
const TicketPrice = require('./ticket-price');
const db = require('./../../db/db');

module.exports = class TicketState {
    constructor(fileName, queryDateString, showDateString) {
        this.queryDateString = queryDateString;
        this.showDateString = showDateString;
        this.scheduleObjects = schowScheduleGetter.getShowShedule(fileName, showDateString);
    }

    getStatesForCommand() {
        let addPrice = false;
        let showStatesGroupedByGenre = getShowStatesGroupedByGenre(
            this.scheduleObjects, this.queryDateString, this.showDateString, addPrice
        );

        let invertory = {
            'inventory': showStatesGroupedByGenre
        };

        return invertory;
    }

    getStatesWithPrice(queryDateString, showDateString) {
        let addPrice = true;
        let showStatesGroupedByGenre = getShowStatesGroupedByGenre(
            this.scheduleObjects, this.queryDateString, this.showDateString, addPrice
        );
        return showStatesGroupedByGenre;
    }
};

//private methods
function getShowStatesGroupedByGenre(scheduleObjects, queryDateString, showDateString, addPrice) {
    let showStates = scheduleObjects.map((show) => {
        return getTicketStateForOneShow(show, queryDateString, showDateString, addPrice);
    });

    showStates = removeStartDatesFromShowStates(showStates);
    let showStatesGroupedByGenre = showStates.reduce(groupByGenreCallback, []);

    return showStatesGroupedByGenre;
}

function getTicketStateForOneShow(show, queryDateString, showDateString, addPrice) {
    let ticketDateState = new TicketDateState(
        show.startDateString, queryDateString, showDateString
    );
    let ticketAvailability = new TicketAvailability(ticketDateState);
    
    let ticketPriceGetter = new TicketPrice(show.genre);

    let availableAmount = ticketAvailability.getAvailableTicketAmount();
    let leftAmount = ticketAvailability.getLeftTicketAmount();
    let saleStatus = ticketAvailability.getSaleStatus();
    let ticketPrice = ticketPriceGetter.getPriceByGenre();

    if (ticketDateState.isShowDiscounted()) {
        ticketPrice = ticketPriceGetter.getDiscountedPrice();
    }

    show['tickets left'] = leftAmount;
    show['tickets available'] = availableAmount;
    show['status'] = saleStatus;
    if (addPrice) {
        show['price'] = ticketPrice;
        show['tickets sold'] = db.getSoldTickets(show.title, queryDateString, showDateString);
    }

    return show;
}

function groupByGenreCallback(accumulator, currentValue) {
    for (let i = 0; i < accumulator.length; i++) {
        if (accumulator[i]['genre'] === currentValue['genre']) {
            //we do not need genre because we group by genre
            delete currentValue['genre'];
            accumulator[i]['shows'].push(currentValue);
            return accumulator;
        }
    }
    //if we did not find a genre we create a new one
    accumulator.push({'genre': currentValue['genre'], 'shows': [currentValue]});

    return accumulator;
}

function removeStartDatesFromShowStates(showStates) {
    let showStatesWithoutDates = showStates.map(showState => {
        delete showState['startDateString'];

        return showState;
    });
    return showStatesWithoutDates;
}