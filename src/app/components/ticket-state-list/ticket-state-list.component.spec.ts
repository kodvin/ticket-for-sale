import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketStateListComponent } from './ticket-state-list.component';

describe('TicketStateListComponent', () => {
  let component: TicketStateListComponent;
  let fixture: ComponentFixture<TicketStateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketStateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketStateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
