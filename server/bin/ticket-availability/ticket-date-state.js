const moment = require('moment');
const constants = require('./../constants');

//class for setting up all relevant dates for the show
module.exports = class TicketDateState {
    constructor(startDateString, queryDateString, showDateString) {
        //date when the show starts
        this.startDate = moment(startDateString, constants.dateFormat);
        //date of reference day
        this.queryDate = moment(queryDateString, constants.dateFormat);
        //date of show date, date that we want to buy tickets or get how many are available
        this.showDate = moment(showDateString, constants.dateFormat);
        //date when the show ends
        this.endDate = this.startDate.clone().add(constants.showRunningDays, 'days');
        //date event moves from big hall to small hall
        this.endBigHallDate = this.startDate.clone().add(constants.bigHallRunnigDays, 'days');
        //date when discount starts for small hall
        this.startDiscountDate = this.startDate.clone().add(constants.tillDiscountDays, 'days');
        //date when sale starts, fixed amount of days before the show day
        this.startSaleDate = this.showDate.clone().subtract(constants.daysBeforeSaleStart, 'days');
    }

    isShowDateAfterEndDate() {
        return moment(this.showDate).isAfter(this.endDate, 'day');
    }

    ticketsOnSale() {
        //sale has not started: if query date is before the sale started
        if (moment(this.queryDate).isBefore(this.startSaleDate, 'day')) return false;
        //show ended: if show date is after end date
        if (moment(this.showDate).isAfter(this.endDate, 'day')) return false;
        //show has not started yet: if show date is before the show starts then tickes ar not on sale
        if (moment(this.showDate).isBefore(this.startDate, 'day')) return false;

        return true;
    };

    soldTickets() {
        let daysBeforeShow = this.showDate.clone().diff(this.queryDate, 'days');

        //sale has not started
        if (daysBeforeShow > constants.daysBeforeSaleStart) return 0;
        //show is in the past
        if (daysBeforeShow < 0) return 0;
        //if show date is before the show starts then tickes ar not on sale
        if (moment(this.showDate).isBefore(this.startDate, 'day')) return 0;

        //plius 1 mecause we can sell on the same day as the sale started
        let numberOfSaleDays = constants.daysBeforeSaleStart - daysBeforeShow;
        let availableTicketPerDay = this.availableTicketPerDay();
        let ticketsSold = numberOfSaleDays * availableTicketPerDay;
        //cannot sell more tickets than the capacity
        if (ticketsSold > this.capacityForShowDate()) {
            return this.capacityForShowDate();
        }
        return ticketsSold;
    };

    capacityForShowDate() {
        if (this.isShowDateInBigHall()) return constants.bigHallCapacity;
        if (this.isShowDateInSmallHall()) return constants.smallHallCapacity;

        return 0;
    };

    availableTicketPerDay() {
        if (this.isShowDateInBigHall()) return constants.bigHallTicketPerDayLimit;
        if (this.isShowDateInSmallHall()) return constants.smallHallTicketPerDayLimit;

        return 0;
    };

    isShowDateInBigHall() {
        //show has not started
        if (moment(this.showDate).isBefore(this.startDate, 'day')) return false;
        //show moved to small hall or ended
        if (moment(this.showDate).isAfter(this.endBigHallDate, 'day')) return false;

        return true;
    };

    isShowDateInSmallHall() {
        //show has not started
        if (moment(this.showDate).isBefore(this.startDate, 'day')) return false;
        //show has ended
        if (moment(this.showDate).isAfter(this.endDate, 'day')) return false;

        return true;
    };

    isShowDiscounted() {
        //discount has not started
        if (moment(this.showDate).isBefore(this.startDiscountDate, 'day')) return false;
        //show has ended
        if (moment(this.showDate).isAfter(this.endDate, 'day')) return false;

        return true;
    };
}