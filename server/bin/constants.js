module.exports = {
    showRunningDays: 100,
    bigHallRunnigDays: 60,
    tillDiscountDays: 80,
    bigHallCapacity: 200,
    smallHallCapacity: 100,
    daysBeforeSaleStart: 25,
    bigHallTicketPerDayLimit: 10,
    smallHallTicketPerDayLimit: 5,
    dateFormat: "YYYY-MM-DD",
    isStrictDateParsing: true,
    discountInPercents: 20,
    genrePricesDollars: [
        {"genre": "musical", "priceInDollars": 70},
        {"genre": "comedy", "priceInDollars": 50},
        {"genre": "drama", "priceInDollars": 40}
    ]
};