# Tickets4sale

## Project structure and design

Project was written in fullstack javascript with nodejs(express.js) as backend and Angular 6 as frontend.
In this setup onboarding is easier because only one programming language is needed.
Only one package manager - npm. Front and back parts of the app are managed via one manager.
With this angular version there is no need to manage assets(css, js, images) manually. 
Besides angular cli makes new components/routes generation faster.
App is written in a way that the backend can be easily changed if the need arises.

## Environment 
Install nodejs with npm. instructions can be found gere -> https://www.npmjs.com/get-npm
Run `npm install @angular/cli -g`
To install packages run `yarn install`
In working directory `npm link`
For cleanup `npm unlink` and `npm uninstall @angular/cli -g`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Development server
Install nodemon with 'npm install -g nodemon'. It will install nodemon globally 
so to remove it execute "npm uninstall -g nodemon"
Run `nodemon server.js` for a dev server. Navigate to `http://localhost:3000/`. 


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io) for fronetend tests.

## Running cli command

Go to working directory of this project.
Execute `node state-query.js <filename> <queryDate> <showDate>`
fileName is the file name of csv which should be placed in  "<projectDirectory>/server/resources/"	
For example `node state-query.js shows.csv 2017-01-14 2018-01-23`
