import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-show-date-selection',
  templateUrl: './show-date-selection.component.html',
  styleUrls: ['./show-date-selection.component.css']
})
export class ShowDateSelectionComponent {

    @Input() isDateValid: boolean;
    @Output() requestTicketState = new EventEmitter();
    @Input() showDate = new Date();

    requestState() {
        this.requestTicketState.emit(this.showDate);
    }
}
