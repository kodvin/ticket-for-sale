import {TicketState} from './ticket-state';

export class TicketStateByGenre {
    genre: string;
    shows: TicketState[];
} 