const loki = require('lokijs');

let db = new loki('shows.loki.db');
let showsCollection = db.getCollection('showsCollection');
if (!showsCollection) {
    showsCollection = db.addCollection('showsCollection');
}

// emulated databaseless storage
module.exports = {
    saveSoldTickets: function (title, queryDateString, showDateString, soldTickets) {
        let entry = showsCollection.findOne({title, queryDateString, showDateString});
        if (entry) {
            entry.soldTickets = soldTickets;
            showsCollection.update(entry);
        } else {
            showsCollection.insert({title, queryDateString, showDateString, soldTickets});
        }
        db.saveDatabase();

    },
    getSoldTickets: function (title, queryDateString, showDateString) {
        let entry = showsCollection.findOne({title, queryDateString, showDateString});
        if (entry) return entry.soldTickets;

        return 0;
    },
};

