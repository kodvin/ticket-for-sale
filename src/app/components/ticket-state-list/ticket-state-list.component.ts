import {Component} from '@angular/core';
import * as CONSTANTS from '../../app.constants';
import {TicketStateByGenre} from '../../model/ticket-state-by-genre';
import {ApiService} from '../../services/api.service';
import * as moment from 'moment';

@Component({
    selector: 'ticket-state-list',
    templateUrl: './ticket-state-list.component.html',
    styleUrls: ['./ticket-state-list.component.css']
})
export class TicketStateListComponent {

    private apiHost: string = CONSTANTS.BACKEND_API_URL;
    public ticketStateByGenre: TicketStateByGenre[] = [];
    public showDate: any;
    public isDateValid = true;

    constructor(private api: ApiService) {
    }

    showDateChangeConfirmed(unformatedShowDateString): void {
        this.showDate = unformatedShowDateString;
        this.requestTicketStates();
    }

    requestTicketStates(): void {
        this.isDateValid = true;
        const showDate = moment(this.showDate);
        if (!showDate.isValid()) {
            this.isDateValid = false;
            return;
        }
        const showDateString = showDate.format(CONSTANTS.DATE_FORMAT);
        this.getTicketStates(showDateString);
    }

    private getTicketStates(showDate: string): void {
        const path = '/api/ticket/state/' + showDate;
        this.api
            .get(path)
            .subscribe(
                (newTicketStateByGenre: any) => {
                    this.ticketStateByGenre = newTicketStateByGenre;
                },
                error => console.error('error --', error)
            );
    }
}
