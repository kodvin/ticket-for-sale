const TicketState = require('./server/bin/ticket-availability/ticket-state');
const validation = require('./server/bin/validation');

if (process.argv.length < 5) {
	console.log(`Not all arguments were supplied Required in order: <csv_file_name> <query_date> <show_date>`);
	return;
}

let csvFileName = process.argv[2];
let queryDate = process.argv[3];
let showDate = process.argv[4];

if (!validation.isDateStringValid(queryDate)) {
	console.log("queryDate is not in a valid format");
	return;
}
if (!validation.isDateStringValid(showDate)) {
	console.log("showDate is not in a valid format");
	return;
}

if (!validation.isSecondDateAfterFirst(queryDate, showDate)) {
	console.log("queryDate cannot bet futher in time than showdate");
	return;
}


let ticketState = new TicketState(csvFileName, queryDate, showDate);
let queryResult = ticketState.getStatesForCommand();
let spacesForFormating = 4;
console.log(JSON.stringify(queryResult, null, spacesForFormating));