import { Component, Input, Output, EventEmitter } from '@angular/core';
import {TicketState} from '../../model/ticket-state';
import * as moment from 'moment';
import * as CONSTANTS from '../../app.constants';
import {ApiService} from '../../services/api.service';


@Component({
  selector: '[app-ticket-state]',
  templateUrl: './ticket-state.component.html',
  styleUrls: ['./ticket-state.component.css']
})
export class TicketStateComponent {

	@Input() ticketState: TicketState; 
	@Input() showDate: any; 
	@Output() ticketsBought = new EventEmitter();

  	constructor(private api: ApiService) { }

	buyTickets(title: string, startDateString: string, numberOfTickets: number) {
	    const path = '/api/ticket/buy';
	    const body = {
	        title,
	        numberOfTickets,
	        startDateString,
	        showDateString: moment(this.showDate).format(CONSTANTS.DATE_FORMAT),
	    };
	    this.api
	        .post(path, body)
	        .subscribe(
	            (response: any) => {
	                if (response['status'] === true) {
	                    this.ticketsBought.emit();
	                }
	                alert(response['message']);
	            },
	            error => console.error('error --', error)
	        );
	}
}
