class TicketAvailability {
    constructor(ticketDateState) {
        if (!ticketDateState) {
            throw "TicketDateState object not provided";
        }
        this.ticketDateState = ticketDateState;
    }

    getLeftTicketAmount() {
        let capacity = this.ticketDateState.capacityForShowDate();
        let soldTickets = this.ticketDateState.soldTickets();
        let leftTickets = capacity - soldTickets;

        return leftTickets;
    };

    getAvailableTicketAmount() {
        //if tickets are sold out they are not available
        if (this.getLeftTicketAmount() === 0) return 0;
        //sale has not started or show already ended
        if (!this.ticketDateState.ticketsOnSale()) return 0;

        return this.ticketDateState.availableTicketPerDay()
    };

    getSaleStatus() {
        if (this.ticketDateState.ticketsOnSale() && this.getLeftTicketAmount() === 0) {
            return "Sold out";
        } else if (this.ticketDateState.ticketsOnSale() && this.getLeftTicketAmount() !== 0) {
            return "Open for sale";
        }
        if (this.ticketDateState.isShowDateAfterEndDate()) return "In the past";

        //if show date is in the interval or show running period and tickets are not on sale
        //that means sale has not started yet
        if (
            (this.ticketDateState.isShowDateInBigHall() || this.ticketDateState.isShowDateInSmallHall()) &&
            !this.ticketDateState.ticketsOnSale()
        ) return "Sale not started";

        return 'No show on that date';
    };


}

module.exports = TicketAvailability; 