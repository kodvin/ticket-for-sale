import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import * as CONSTANTS from '../app.constants';


const OPTIONS = {
    withCredentials: true,
};


@Injectable()
export class ApiService {

    private apiHost: string = CONSTANTS.BACKEND_API_URL;

    constructor(private http: HttpClient) {
    }

    public get(path: string): Observable<Response> {
        return this.http.get<any>(`${this.apiHost}${path}`, OPTIONS)
            .catch((err: Response) => {
                console.log(err['error']['text']);
                const details = err.json();
                return Observable.throw(details);
            });
    }

    public post(path: string,
                body: any = undefined): Observable<Response> {
        return this.http.post<any>(`${this.apiHost}${path}`, body, OPTIONS)
            .catch((err: Response) => {
                console.log(err);
                const details = err.json();
                return Observable.throw(details);
            });
    }

    public put(path: string,
               body: any = undefined): Observable<Response> {
        return this.http.put<any>(`${this.apiHost}${path}`, body, OPTIONS)
            .catch((err: Response) => {
                console.log(err);
                const details = err.json();
                return Observable.throw(details);
            });
    }

    // 'delete' seems to be a reserved word in typescript
    public del(path: string,
               body: any = undefined): Observable<Response> {
        let options = OPTIONS;
        if (body) {
            options = Object.assign(OPTIONS, {body});
        }
        return this.http.delete<any>(`${this.apiHost}${path}`, options)
            .catch((err: Response) => {
                console.log(err);
                const details = err.json();
                return Observable.throw(details);
            });
    }

}
