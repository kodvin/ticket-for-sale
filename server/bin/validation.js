const moment = require('moment');
const constants = require('./constants');

module.exports = {
    isDateStringValid(dateString) {
        return moment(dateString, constants.dateFormat, constants.isStrictDateParsing).isValid()
    },
    isSecondDateAfterFirst(firstDateString, secondDateString) {
        return moment(secondDateString).isAfter(firstDateString, 'day');
    }
};